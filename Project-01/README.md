# PROJECT 01

This folder contains the files for project01.

1. [project1.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project1.txt)                     : Contains the problem statement as given to us.
2. [project01.py](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project01.py)                     : Python code, which is our solution.
3. [L1Topology.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/L1Topology.txt) , [L2topology.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/L2Topology.txt)  : Inputs to the code which specify the topologies present in the network.
4. [Network.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/Network.txt)                      : Output from the python code which lists each node and their links.

## Input format:
[L1Topology.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/L1Topology.txt) will contain only 1 line, which specifies the higher order topology of the network. This line will be the form:
`X, n, m`, where the notation is the same as given in the [problem statement](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project1.txt). [L2topology.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/L2Topology.txt) describes each "tile"  of the network as we call it, which itself is a network of nodes. Each line in [L2topology.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/L2Topology.txt) will be of the same format as that of the single line in [L1Topology.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/L1Topology.txt). **Note** that we require each line to follow the correct format and must have all three parameters; in case of a malformatted file, the code will send a message on the standard output terminal, indicating the error faced or assumption made.

## Output format:
The output file is named [Network.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/Network.txt), and describes the graph as required by the problem statement. The format followed for each node is described below:
```
*****
Node Name: <Node Name>
Node ID: <Node ID>
Links: <Number of Links>
	L(1):<Node connected through first link>
    L(2):<Node connected through second link>
    ...
    ...
    L(i):<Node connected through i'th link>
    ...
*****
```
`Node name` follows the following format: the first letter specifies the type of tile topology the node belongs to. The next number specifies the ID number of the tile in the larger network. For Chain, Ring, and Hypercube, tile topologies, the next number indicates the node number in the tile itself. For Mesh and Folded Torus, the next two numbers are indices in a 2D plane. For the Butterfly tiles, there is a substring that follows which specifies whether the node is an input to the Butterfly tile, a switch, or an output node to the Butterfly tile. 
The `node ID` is a global ID assigned to each node, in order of creation. The links use this node ID to indicate the connections. 

